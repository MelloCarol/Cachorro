//
//  ViewController.swift
//  Cachorro
//
//  Created by COTEMIG on 22/09/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Cachorro: Decodable {
    let message: String
    let status: String
}

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    var emEspera = UIActivityIndicatorView()
    override func viewDidLoad() {
        super.viewDidLoad()
        configEmEspera()
        emEspera.startAnimating()
        self.view.isUserInteractionEnabled = false
        getNovoCao()
    }

    @IBAction func recarregarImagem(_ sender: Any) {
        emEspera.startAnimating()
        self.view.isUserInteractionEnabled = false
        getNovoCao()
    }
    func configEmEspera(){
        emEspera.center = self.view.center
        emEspera.hidesWhenStopped = true
        emEspera.style = .large
        emEspera.color = UIColor.gray
        view.addSubview(emEspera)
    }
    
    func getNovoCao(){
        
        AF.request("https://dog.ceo/api/breeds/image/random").responseDecodable(of: Cachorro.self) { (response) in
            self.emEspera.startAnimating()
            self.view.isUserInteractionEnabled = false
            if let cachrro = response.value{
                self.imageView.kf.setImage(with: URL(string: cachrro.message)) { _ in
                    self.emEspera.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                }
                
            }
        }
    }
}

